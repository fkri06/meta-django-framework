from django.apps import AppConfig


class LabdjangoAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'labdjango_app'
