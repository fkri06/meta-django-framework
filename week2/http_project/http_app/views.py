from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(request):
    path = request.path
    scheme = request.scheme
    method = request.method
    user_agent = request.META
    path_info = request.path_info
    msg = f'<p>Path: {path}</p><p>Scheme: {scheme}</p> <p>Method: {method}</p><p>User Agent: {user_agent}</p><p>Path Info: {path_info}</p>'
    response = HttpResponse(msg)
    # return HttpResponse(path, content_type='text/html', charset='utf8')
    return response