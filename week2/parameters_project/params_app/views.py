from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def menuitems(request, dish):
    items = {
        'pasta': 'Pasta is a type of noodle from Italia',
        'cheesecake': 'Type of desert',
        'ramen': 'Ramen is a type of noodle from Japan'
    }

    description = items[dish]

    return HttpResponse(f"<h2> {dish} </h2>" + description)