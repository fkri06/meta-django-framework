from django.shortcuts import render
from django.http import HttpResponse
from random import randint
from .models import Menu

# Create your views here.
def hello(request):
    try:
        menus = list(Menu.objects.all())
        get_random_menu = randint(0, len(menus)-1)
        context = {'random_menu': menus[get_random_menu]}
    except Exception as error:
        print(error)
        return render(request, 'little_lemon/index.html', context={'error': error})
    else:
        return render(request, template_name='little_lemon/index.html', context=context)

    