from django.db import models

# Create your models here.
class Menu(models.Model):
    """ Menu has name and description. """
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self) -> str:
        """ Return a string representation of the model."""
        return self.name
    
